FROM node:12.16.1

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev vim

# Environment variables
ENV NODE_ROOT /usr/app/
ENV WWW_PORT 4200
ENV WEBPACK_PORT 49153

# The default port from ng serve (4200)
# and 49153 for Webpack Hot Module Reload
EXPOSE ${WWW_PORT} ${WEBPACK_PORT}


RUN mkdir -p ${NODE_ROOT}

WORKDIR ${NODE_ROOT}

RUN npm install -g @angular/cli

COPY package.json package.json

RUN npm install \
  && npm cache clean --force

COPY . .

CMD ["npm", "start"]




